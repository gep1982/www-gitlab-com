---
layout: markdown_page
title: "Group Direction - Editor"
description: "Overall direction for the Editor group"
canonical_path: "/direction/create/editor/"
---

- TOC
{:toc}

## The Editor group

**Content last reviewed on 2021-10-29**

This is the direction page for the Editor group which is part of the [Create stage](/direction/dev/index.html#create) of the DevOps lifecycle and is responsible for the following categories: 

|  Category   |   Direction  |  Description | Maturity  |
|  ---   |   ---   |   ---   |  ---  |
| Web IDE | [Direction page](/direction/create/editor/web_ide/) | A web-based multi-file code editor |  [Viable](/direction/maturity/) |
| Snippets | [Direction page](/direction/create/editor/snippets/) | Small, sharable bits of code saved for easy access  |  [Complete](/direction/maturity/) |
| Live Preview | [Direction page](/direction/create/editor/live_preview/) | Preview JavaScript and static sites in real-time in the Web IDE |  [Minimal](/direction/maturity/) |
| Wiki | [Direction page](/direction/create/editor/wiki/) | Built-in, git-backed knowledge base |  [Viable](/direction/maturity/) |
| Static Site Editor | [Direction page](/direction/create/editor/static_site_editor/) | Visual editor for Markdown content hosted in a static site |  [Viable](/direction/maturity/) |

In support of these categories, the Editor group created and maintains two core editing components. These editors are intended to be flexible, extensible, and reusable across GitLab and while they don't meet the traditional definition of a category, they do have distinct strategies and backlogs. They are: 

| Component | Direction | Description | Strategy epic |
|  ---  |  ---  |  ---  |  ---  |
| Source Editor | [Direction page](/direction/create/editor/source_editor/) | Our underlying `code` editor |  [Epic](https://gitlab.com/groups/gitlab-org/-/epics/4861)  |
| Content Editor | [Direction page](/direction/create/editor/content_editor/) | A visual Markdown editor |  [Epic](https://gitlab.com/groups/gitlab-org/-/epics/5401)  |

## What are we working on and why?

With 5 categories and 2 foundational editor components, the Editor group has to prioritize aggressively. In general, the group's goals revolve around consolidation and standardization of editing experiences to improve consistency and maintainability. Our current focus areas and engineering investment are broken down by category below, percentages represent how much engineering time on average is allocated in each milestone. Since the Source Editor and Content Editor span multiple categories, investment in those is included in the most relevant category.

#### Web IDE/Source Editor – 50%

The Web IDE has now been [refactored to use Source Editor](https://gitlab.com/groups/gitlab-org/-/epics/3282) which enables us to work on creating a more consistent, cohesive code editing experience to GitLab. One of the primary goals for the category is to bridge the gap between the Single File Editor (the experience of editing a single file from the Repository view) and the more complex but powerful Web IDE. We believe that breaking down the barrier between these two editing paths will drive adoption and overall satisfaction with the merge request and review experience. While we tackle that larger concern, we will also focus on extending the underlying Source Editor itself and enabling more advanced workflows like [live Markdown preview](https://gitlab.com/groups/gitlab-org/-/epics/4859) or [inline merge request discussions](https://gitlab.com/groups/gitlab-org/-/epics/72). 

#### Wiki/Content Editor – 50%

The Wiki is one of our most used categories and there are several opportunities to improve the experience and drive growth. The largest and most strategically important opportunity is the [introduction of a WYSIWYG Markdown editor](https://gitlab.com/groups/gitlab-org/-/epics/5403), what we've come to call the Content Editor, which shipped in MVC form in GitLab 14.0. The highest priority for the Editor group here is to build [complete support for GitLab Flavored Markdown](https://gitlab.com/groups/gitlab-org/-/epics/5438) so that the Content Editor can be used as a production-ready, default editing experience for Wiki. Afterward, the Content Editor can be introduced anywhere Markdown content is written in GitLab. This includes issue and epic descriptions, comments, and even when editing Markdown files directly in the Repository view or Web IDE. 

#### Snippets – 0-5%

Snippets reached Complete maturity in late 2020 with the introduction of [versioned](https://gitlab.com/groups/gitlab-org/-/epics/239) and [multi-file snippets](https://gitlab.com/groups/gitlab-org/-/epics/2829). Snippets are powered by Source Editor, so improvements to that experience can in turn benefit the editing experience of Snippets. The next category-specific features we would like to focus on are around [organization](https://gitlab.com/groups/gitlab-org/-/epics/3204) and [sharing](https://gitlab.com/groups/gitlab-org/-/epics/1496) but the team will focus first on categories with larger user bases and wider potential reach.

#### Live Preview – 0%

Live Preview is a valuable feature for a subset of JavaScript-based or completely static web projects. The path to wider adoption hinges on the ability to offer server-side compilation of code or remote development environments, but that is something that will take significant research and investment. For now, we are letting Live Preview exist as-is, and we will continue to evaluate its place alongside the Web IDE as the editor experience itself matures.  

#### Static Site Editor – 0%

The Static Site Editor reached Viable maturity in late 2020, however we identified a need for a more scalable and extensible WYSIWYG editor. This [technical investigation](https://gitlab.com/gitlab-org/gitlab/-/issues/231725) resulted in what is now known as the Content Editor — an HTML-based rich text editor that is currently implemented in the Wiki. Our intention is to bring that editor back to the Static Site Editor or, potentially, use the Content Editor in a slightly different form to solve the same jobs to be done that the Static Site Editor was meant to solve.

## Recent opportunity canvases (GitLab internal)

1. [Quickly get started developing in standardized environments](https://docs.google.com/document/d/1t1j98Wl1erG9b8cUT77yDsNUEPvCOMOp0ktbOkYZfWc/edit#heading=h.4mt5fmtn0ax4)
1. [Build a first-class rich text editor in GitLab](https://docs.google.com/document/d/1L84Z0gdno_WenZ9QWDhuiSsJ0YrPosxUJor7ixtUnZ8/edit)

## Links and resources

- [Editor Group HQ - group strategy epic](https://gitlab.com/groups/gitlab-org/-/epics/5065)
- [Editor Team Handbook page](https://about.gitlab.com/handbook/engineering/development/dev/create-editor/)
- [GitLab Unfiltered Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrRQhnSYRNh1s1mEUypx67-)
