---
layout: handbook-page-toc
title: "Professional Services Instructional Design and Development"
description: "Discover how the GitLab Instructional Design and Development team works together."
---

## Current Offerings
The GitLab Professional Education Services team currently provides the following offerings:

- [Live Instructor-led training (ILT)](https://about.gitlab.com/services/education/)
- [Self Paced training](https://about.gitlab.com/learn/)
- [GitLab Technical Certifications](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/gitlab-technical-certifications/) as Professional Service offerings. 
- [Train-the-Trainer](https://about.gitlab.com/services/education/train-the-trainer/)
- [Custom Training](https://about.gitlab.com/services/specialized/)

## What's in Progress?

If you want to see what is currently being developed or planned out, check out the [Education Services Activity Board](https://gitlab.com/gitlab-com/customer-success/professional-services-group/education-services/activity)

- GitLab System Administration Specialist
- GitLab System Administration Professional 
- GitLab CI/CD Professional 
- GitLab Integration Services Engineer Specialist


## WorkFlow Labels for Education Services Issues

Here are the main labels used for PS instructional development projects.

- `ProServ-practice::Education`- used for all education service items.
- `Workflow::scheduling` - ID begins the storyboarding phase and begins planning out the course development.
- `Workflow::ready for development` - ID determines project is ready for development work to begin and builds issues and epics for work tracking. 
- `Workflow::in dev` - The individual components for the Education Services solution are being designed and developed. 
- `Workflow::ready for review` - The ID distributes the drafts to the SMEs and approved review team for approvals.
- `Workflow::in review` - The ID completes any necessary edits and/or additions that were determined necessary during the 'ready for review' stage.
- `Workflow::blocked` - not used for ID purposes.
- `Workflow::verification` - This represents the pilot delivery phase for an educational offering
- `Workflow::staging` - The ID begins the handbook additions and/or wiki instructions for any course changes/additions prior to implementation.  
- `Workflow::production` - The Education Services solution is live and can be accessed by the target audience


### Have an Idea for a New Training?
To submit an Education Request, create a new issue and select the [education-request template](https://gitlab.com/gitlab-com/customer-success/professional-services-group/education-services/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) and fill out the necessary details.
